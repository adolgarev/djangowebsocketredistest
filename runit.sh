#!/bin/sh

pip install --upgrade pip
pip install -r requirements.txt
python manage.py migrate --noinput
python manage.py compilemessages
python manage.py collectstatic --noinput
uwsgi --http-socket uwsgi_ws.sock --chmod-socket --gevent 1000 --http-websockets --workers=2 --master --module DjangoWebsocketRedisTest.wsgi_ws &
exec uwsgi --touch-reload reload --http-socket uwsgi.sock --chmod-socket --master --module "django.core.wsgi:get_wsgi_application()" --processes 2
