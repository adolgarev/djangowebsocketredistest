import datetime

from django.shortcuts import render
from django.http import HttpResponse

from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage


def test(request):
    return render(request, "test.html")


def send(request):
    redis_publisher = RedisPublisher(facility='foobar', broadcast=True)
    messageString = datetime.datetime.now().__str__()
    redis_publisher.publish_message(RedisMessage(messageString))
    return HttpResponse(messageString)